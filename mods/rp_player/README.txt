Repixure core mod: rp_player
=============================
By Kaadmy and Wuzzy, for Repixture

Adds a configurable player model for Repixture.

Developers: See `API.md` to manage the player model (including textures and animation).

Sound license: CC0
Texture license: CC BY-SA 4.0
Model license: CC BY-SA 4.0
Source license: LGPLv2.1
