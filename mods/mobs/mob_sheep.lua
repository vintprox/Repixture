
-- Sheep by PilzAdam; tweaked by Kaadmy

local S = minetest.get_translator("mobs")

local mod_nav = minetest.get_modpath("rp_nav")

local top_grass = {
   ["rp_default:grass"] = "air",
   ["rp_default:tall_grass"] = "rp_default:grass",
   ["rp_default:swamp_grass"] = "air",
   ["rp_default:dry_grass"] = "air",
}

mobs:register_mob(
   "mobs:sheep",
   {
      type = "animal",
      mob_name = S("Sheep"),
      passive = true,
      hp_min = 10,
      hp_max = 14,
      breath_max = 5,
      armor = 200,
      collisionbox = {-0.5, -1, -0.5, 0.5, 0.1, 0.5},
      visual = "mesh",
      mesh = "mobs_sheep.x",
      visual_size = {x = 1, y = 1},
      textures = {
	 {"mobs_sheep.png"},
      },
      gotten_texture = {"mobs_sheep_shaved.png"},
      --      gotten_mesh = "mobs_sheep.x",
      makes_footstep_sound = true,
      sounds = {
	 random = "mobs_sheep",
	 death = "mobs_sheep",
	 damage = "mobs_sheep",
	 eat = "mobs_eat",
	 distance = 16,
      },
      walk_velocity = 1,
      walk_chance = 150,
      jump = false,
      jump_height = 5,
      drops = {
	 {name = "mobs:meat_raw",
	  chance = 1, min = 2, max = 4},
	 {name = "mobs:wool",
	  chance = 1, min = 1, max = 2, no_drop_if_gotten = true},
      },
      water_damage = 0,
      lava_damage = 5,
      light_damage = 0,
      animation = {
	 speed_normal = 15,
	 speed_run = 25,
	 stand_start = 0,
	 stand_end = 60,
	 walk_start = 61,
	 walk_end = 80,
      },
      follow = "rp_farming:wheat",
      view_range = 5,
      -- Replacements: Eat grass from the ground
      replace_rate = 1000,
      replace_what = {
         "rp_default:dirt_with_grass",
         "rp_default:dirt_with_dry_grass",
         "rp_default:dirt_with_swamp_grass",
      },
      replace_with = "rp_default:dirt",
      replace_offset = -2,

      on_replace = function(self, pos)
         local nr = minetest.get_node(pos)
         -- Check node above
         local above = {x=pos.x,y=pos.y+1,z=pos.z}
         local na = minetest.get_node(above)

         -- Eat grass
         if na.name == "air" then
            -- Eat grass from dirt node
            if nr.name == "rp_default:dirt_with_swamp_grass" then
               minetest.set_node(pos, {name = "rp_default:swamp_dirt"})
            else
               minetest.set_node(pos, {name = self.replace_with})
            end
         elseif top_grass[na.name] then
            -- If grass plant on top, eat it first
            minetest.set_node(above, {name = top_grass[na.name]})
         else
            return
         end

         -- Regrow wool
         if mobs:feed_tame(self, self.follow, 8, false, false) then
            if self.gotten == true then
               self.object:set_properties(
                 {
                    textures = {"mobs_sheep.png"},
                    mesh = "mobs_sheep.x",
               })
               self.gotten = false
            end
         end
      end,
      on_rightclick = function(self, clicker)

         local item = clicker:get_wielded_item()
         local itemname = item:get_name()

         -- Demagnetize magnocompass if sheep has wool
	 if mod_nav then
            local compass_group = minetest.get_item_group(itemname, "nav_compass")
	    if compass_group > 0 then
               if compass_group == 2 and self.gotten == false then
	          item = nav.demagnetize_compass(item, clicker:get_pos())
                  clicker:set_wielded_item(item)
                  return
               end
               return
            end
	 end

         -- Are we feeding?
         if mobs:feed_tame(self, clicker, 8, true) then
            return
         end

         -- Are we giving a haircut?

         if minetest.get_item_group(itemname, "shears") > 0 then
            if self.gotten == false and self.child == false then
               self.gotten = true -- shaved
               local pos = self.object:get_pos()
               pos.y = pos.y + 0.5
               local obj = minetest.add_item(pos, ItemStack("mobs:wool"))
               minetest.sound_play({name = "default_shears_cut", gain = 0.5}, {pos = clicker:get_pos(), max_hear_distance = 8}, true)
               if obj then
                  obj:set_velocity(
                     {
                        x = math.random(-1,1),
                        y = 5,
                        z = math.random(-1,1)
                  })
               end
               if not minetest.is_creative_enabled(clicker:get_player_name()) then
                   local def = item:get_definition()
                   local cuts = minetest.get_item_group(itemname, "sheep_cuts")
                   if cuts > 0 then
                      item:add_wear_by_uses(cuts)
                   else
                      item:add_wear_by_uses(def.tool_capabilities.snappy.uses)
                   end
               end
               clicker:set_wielded_item(item)
               self.object:set_properties(
                  {
                     textures = {"mobs_sheep_shaved.png"},
                     mesh = "mobs_sheep.x",
               })
               achievements.trigger_achievement(clicker, "shear_time")
            end

            return
         end

         -- Are we capturing?

         mobs:capture_mob(self, clicker, 0, 5, 60, false, nil)
      end,

})

mobs:register_spawn(
   "mobs:sheep",
   {
      "rp_default:dirt_with_grass"
   },
   20,
   10,
   15000,
   1,
   31000
)

mobs:register_egg("mobs:sheep", S("Sheep"), "mobs_sheep_inventory.png")
