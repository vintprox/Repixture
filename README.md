# Repixture

Repixture is a sandbox survival crafting and building game focusing on low-tech.
Explore random worlds in a temperate climate and gather resources to
survive, create tools, build a home and explore the various possibilities of
the world.

**Player Manual**: <https://wuzzy.codeberg.page/Repixture>

## Version

3.9.0

Designed for use with Minetest 5.6.0 or later.

## Features

* Animals and monsters
* Villages
* Village people: They give you hints and want to trade
* Farming and animal breeding
* Upgradable tools via jeweling
* Hunger
* Armor
* Simple crafting system with crafting guide
* A variety of trees and biomes
* Weather
* Beds (skip the night)
* Boats
* Change your player appearance
* Achievements
* Multi-language support
* Creative Mode (unlimited building)
* Over 250 items in total

## Credits

Repixture was started by Wuzzy. It's a fork of Pixture.
Repixture is a revival of Pixture 0.1.1, a game for Minetest 0.4.

Pixture is Copyright (C) 2015-2017 [Kaadmy](https://github.com/kaadmy).

Pixture was inspired by [Kenney](http://kenney.nl).

### Core developers

* Wuzzy: Core development of Repixture
* Kaadmy: Core development of Pixture 2015-2017

### Textures

* Sounds in the `rp_default` mod are all by Kenney (CC0)
* All textures/models by Kaadmy, with some additions/changes by Wuzzy (CC BY-SA 4.0)
   * Exception: Seagrass by jp (CC0)
   * Exception: Barrel, by Wuzzy based on work by jp (CC0)

### Translators

* Wuzzy: German
* rudzik8: Russian

### Sounds

* There is a large number of authors
* See the individual README files in the mods for details
* Note: All sounds are compatible with CC BY-SA 4.0

### Special thanks

* [Kenney](http://kenney.nl) for the inspiration, most of the aesthetic.

## Licenses

This game is free software, licensed 100% under free software licenses.

See *LICENSE.txt* or the links below for the full license texts.

- Media files: all licensed under CC BY-SA 4.0 or CC0, with one exception:
    - The exception: Sounds in the `rp_weather` mod are under GPLv2
    - See per-mod READMEs for details
- Source code: all licensed under LGPLv2.1 (or later versions of the LGPL), or MIT License, see per-mod READMEs.

Links:
- CC BY-SA 4.0: <https://creativecommons.org/licenses/by-sa/4.0>
- CC0: <https://creativecommons.org/publicdomain/zero/1.0>
- GPLv2: <https://www.gnu.org/licenses/old-licenses/gpl-2.0>
- LGPLv2.1: <https://www.gnu.org/licenses/old-licenses/lgpl-2.1>
- MIT License: <https://mit-license.org/>
